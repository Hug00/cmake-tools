# Colors definition
if(NOT WIN32)
  string(ASCII 27 Esc)
  set(ColourReset "${Esc}[m")
  set(ColourBold  "${Esc}[1m")
  set(Red         "${Esc}[31m")
  set(Green       "${Esc}[32m")
  set(Yellow      "${Esc}[33m")
  set(Blue        "${Esc}[34m")
  set(Magenta     "${Esc}[35m")
  set(Cyan        "${Esc}[36m")
  set(White       "${Esc}[37m")
  set(BoldRed     "${Esc}[1;31m")
  set(BoldGreen   "${Esc}[1;32m")
  set(BoldYellow  "${Esc}[1;33m")
  set(BoldBlue    "${Esc}[1;34m")
  set(BoldMagenta "${Esc}[1;35m")
  set(BoldCyan    "${Esc}[1;36m")
  set(BoldWhite   "${Esc}[1;37m")
endif()

# Cmake macro that sets SO version numbers into cmake variables
# Variables:	- FULL_SO_VERSION
#				- MAJOR_SO_VERSION
#				- MEDIUM_SO_VERSION
#				- MINOR_SO_VERSION
macro(SET_SO_VERSION)
	execute_process(COMMAND ${CMAKE_SOURCE_DIR}/modules/cmake-tools/utils/get_soversion full
			WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
			OUTPUT_VARIABLE FULL_SO_VERSION)

	execute_process(COMMAND ${CMAKE_SOURCE_DIR}/modules/cmake-tools/utils/get_soversion major
                        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                        OUTPUT_VARIABLE MAJOR_SO_VERSION)

    execute_process(COMMAND ${CMAKE_SOURCE_DIR}/modules/cmake-tools/utils/get_soversion medium
                        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                        OUTPUT_VARIABLE MEDIUM_SO_VERSION)

    execute_process(COMMAND ${CMAKE_SOURCE_DIR}/modules/cmake-tools/utils/get_soversion minor
                        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                        OUTPUT_VARIABLE MINOR_SO_VERSION)

	message(STATUS "${Yellow}Set VERSION to ${FULL_SO_VERSION} and Major=${MAJOR_SO_VERSION}, Medium=${MEDIUM_SO_VERSION} Minor=${MINOR_SO_VERSION}${ColourReset}")
endmacro(SET_SO_VERSION)
