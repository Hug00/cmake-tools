#
# Find the CppUnit includes and library
#
# This module defines
# CPPUNIT_INCLUDE_DIR, where to find tiff.h, etc.
# CPPUNIT_LIBRARIES, the libraries to link against to use CppUnit.
# CPPUNIT_FOUND, If false, do not try to use CppUnit.

# also defined, but not for general use are
# CPPUNIT_LIBRARY, where to find the CppUnit library.
# CPPUNIT_DEBUG_LIBRARY, where to find the CppUnit library in debug mode.

FIND_PATH(CPPUNIT_INCLUDE_DIR cppunit/TestCase.h
	  PATHS /usr/include/ /usr/local/include/)

FIND_LIBRARY(CPPUNIT_LIBRARY cppunit
	     PATHS ${CPPUNIT_INCLUDE_DIR}/../lib/
	     	   /usr/local/lib/ /usr/lib/)

FIND_LIBRARY(CPPUNIT_DEBUG_LIBRARY cppunit
	     PATHS ${CPPUNIT_INCLUDE_DIR}/../lib/
		   /usr/local/lib/ /usr/lib/)

IF(CPPUNIT_INCLUDE_DIR)
	MESSAGE(STATUS "${Green}CppUnit headers found${ColourReset}")
	IF(CPPUNIT_LIBRARY)
		SET(CPPUNIT_FOUND "YES")
		MESSAGE(STATUS "${Green}CppUnit libraries found${ColourReset}")
		SET(CPPUNIT_LIBRARIES ${CPPUNIT_LIBRARY} ${CMAKE_DL_LIBS})
		SET(CPPUNIT_DEBUG_LIBRARIES ${CPPUNIT_DEBUG_LIBRARY} ${CMAKE_DL_LIBS})
	ELSE(CPPUNIT_LIBRARY)
		MESSAGE(STATUS "${Red}CppUnit libraries not found${ColourReset}")
	ENDIF(CPPUNIT_LIBRARY)
ELSE(CPPUNIT_INCLUDE_DIR)
	MESSAGE(STATUS "${Red}CppUnit headers not found${ColourReset}")
ENDIF(CPPUNIT_INCLUDE_DIR)
